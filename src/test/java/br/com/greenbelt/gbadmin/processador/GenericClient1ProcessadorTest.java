package br.com.greenbelt.gbadmin.processador;

import br.com.caelum.vraptor.observer.upload.DefaultUploadedFile;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GenericClient1ProcessadorTest {

    private GenericClient1Processador genericClient1Processador;

    @Before
    public void setUp() {
        genericClient1Processador = new GenericClient1Processador();
    }
    
    @Test
    public void deveExtrairRubricas() throws IOException, URISyntaxException {
        
        String fileName = "/Users/thiagojfg/IdeaProjects/processador-folha-pagamento/src/test/resources/GENERIC_CLIENT_1.PDF";
        
        InputStream inputStream = new FileInputStream(fileName);
        UploadedFile uploadedFile = new DefaultUploadedFile(inputStream, fileName, "application/pdf", Long.MAX_VALUE);
        
        List<Rubrica> rubricas = genericClient1Processador.processar(uploadedFile);

        Assert.assertFalse(rubricas.isEmpty());
        Assert.assertEquals(22, rubricas.size());
    }
}
