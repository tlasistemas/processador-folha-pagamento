/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Thiago
 */
public class HumanistProcessador extends Processador {

    private static final Logger LOGGER = Logger.getLogger(HumanistProcessador.class.getName());
    
    private static final Pattern patternCodigo = Pattern.compile("^([0-9]{1,}- )");
    private static final Pattern patternValor = Pattern.compile("([0-9\\.]{1,},[0-9]{2})");
    
    @Override
    public String getInicioBloco() {
        return "Total da Empresa";
    }

    @Override
    public String getFinalBloco() {
        return "Total de vencimentos";
    }

    @Override
    public String[] getFinaisBlocoAlternativo() {
        return new String[]{"H U M A N I S T Sistemas"};
    }
    
    @Override
    public int getLinhasAposBloco() {
        return 1;
    }

    @Override
    public Rubrica processarLinha(String linha) {
        
        LOGGER.log(Level.INFO, linha);
        
        Matcher matcherCodigo = patternCodigo.matcher(linha);
        Matcher matcherValor = patternValor.matcher(linha);
        
        if(!matcherCodigo.find()){
            return null;
        }
        
        matcherValor.find();
        
        String codigo = matcherCodigo.group(1);
        String valor = matcherValor.group(1);
        
        String descricao = linha.substring(codigo.length(), linha.indexOf(valor));
        
        codigo = codigo.replaceAll("[^0-9]", "");
        
        return new Rubrica(codigo, descricao, valor);
    }
}
