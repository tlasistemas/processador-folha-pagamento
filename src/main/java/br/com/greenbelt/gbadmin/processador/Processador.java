/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 *
 * @author Thiago
 */
public abstract class Processador {

    private static final Logger LOGGER = Logger.getLogger(Processador.class.getName());
    
    private boolean isPdfFile = false;
    
    protected abstract String getInicioBloco();

    protected abstract String getFinalBloco();
    
    protected String[] getFinaisBlocoAlternativo(){
        return new String[]{};
    }
    
    protected abstract int getLinhasAposBloco();

    private InputStream getInputStream(UploadedFile uploadedFile) throws IOException{
        
        String filename = uploadedFile.getFileName().toLowerCase();
        
        isPdfFile = false;
        
        if(filename.contains("pdf")){
            
            isPdfFile = true;
            
            return extractFromPdf(uploadedFile);
            
        } else {
            
            return uploadedFile.getFile();
            
        }
    }
    
    private InputStream extractFromPdf(UploadedFile uploadedFile) throws IOException{
        
        PDDocument document = PDDocument.load(uploadedFile.getFile());
        
        if (!document.isEncrypted()) {

            String pdfContent = extractPdfContent(document);
            
            document.close();
            
            return new ByteArrayInputStream(pdfContent.getBytes());
        }
        
        throw new IllegalStateException("O arquivo pdf não pode estar criptografado!");
    }
    
    protected String extractPdfContent(PDDocument document) throws IOException{
        
        PDFTextStripper textStripper = new PDFTextStripper();
        
        textStripper.setSortByPosition(true);
        
        return textStripper.getText(document);    
    }
    
    public List<Rubrica> processar(UploadedFile uploadedFile) throws IOException {

        List<Rubrica> rubricas = new ArrayList<>();

        InputStreamReader reader = new InputStreamReader(getInputStream(uploadedFile));

        BufferedReader br = new BufferedReader(reader);

        String linha = br.readLine();

        int linhasAposBloco = 1;

        boolean blocoEncontrado = false;

        while (linha != null) {

            if (!blocoEncontrado) {
                
                blocoEncontrado = isInicioBloco(linha);
                
            } else {

                if (isFinalBloco(linha)) {

                    blocoEncontrado = false;

                    linhasAposBloco = 1;

                } else if (linhasAposBloco < getLinhasAposBloco()) {

                    linhasAposBloco++;

                } else if(StringUtils.isNotEmpty(linha)){                    
                    
                    Rubrica rubrica = processarLinha(linha);
                    
                    if(rubrica != null){
                        rubricas.add(rubrica);
                    }
                }
            }

            linha = br.readLine();
        }

        return rubricas;
    }

    protected abstract Rubrica processarLinha(String linha);

    protected boolean isInicioBloco(String linha) {        
        
        if(StringUtils.containsIgnoreCase(clear(linha), clear(getInicioBloco()))){
            
            LOGGER.log(Level.INFO, "Inicío de Bloco: " + linha);
            
            return true;
            
        } else{
            
            return false;
            
        }
    }

    protected boolean isFinalBloco(String linha) {
        
        String ln = clear(linha);
        
        if(StringUtils.containsIgnoreCase(ln, clear(getFinalBloco()))){
            
            LOGGER.log(Level.INFO, "Fim do Bloco: " + linha);
            
            return true;
        }
                
        for(String finalBloco : getFinaisBlocoAlternativo()){
            
            if(StringUtils.containsIgnoreCase(ln, clear(finalBloco))){
                
                LOGGER.log(Level.INFO, "Fim do Bloco: " + linha);
                
                return true;
            }
        }
        
        return false;
    }

    protected String clear(String linha) {
        return linha.replaceAll(" ", "");
    }
        
    protected final boolean isPdfFile(){
        return isPdfFile;
    }
}
