package br.com.greenbelt.gbadmin.processador.modelo;

import org.apache.commons.lang3.StringUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thiago
 */
public class Rubrica {
    
    private final String codigo;
    
    private final String descricao;
    
    private final String valor;

    public Rubrica(String codigo, String descricao, String valor) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getValor() {
        return valor;
    }
    
    public Double getValorNumeral(){
        
        if(StringUtils.isEmpty(valor)){
            return 0.0;
        }
        
        return Double.valueOf(valor.replace(".", "").replace(",", "."));
    }
}