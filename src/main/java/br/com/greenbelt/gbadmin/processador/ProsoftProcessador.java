/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Thiago
 */
public class ProsoftProcessador extends Processador{
    
    private static final Logger LOGGER = Logger.getLogger(ProsoftProcessador.class.getName());
    
    @Override
    public Rubrica processarLinha(String linha) {
        
        linha = linha.trim();
        linha = linha.replaceFirst(" ", ";");
        linha = linha.replaceAll("(  {2,})", ";");
        
        LOGGER.log(Level.INFO, linha);
        
        String[] partes = linha.split(";");
        
        String codigo = partes[0];
        String descricao = partes[1];
        String total  = partes[partes.length - 1];
        
        return new Rubrica(codigo, descricao, total);
    }
    
    @Override
    public String getInicioBloco() {
        return "RESUMOGERALDAEMPRESA";
    }

    @Override
    public String getFinalBloco() {
        return "=============";
    }

    @Override
    public String[] getFinaisBlocoAlternativo() {
        return new String[]{"F0LHADEPAGAMENTODEEMPREGADOS"};
    }

    @Override
    public int getLinhasAposBloco() {
        return isPdfFile() ? 2 : 3;
    }
}