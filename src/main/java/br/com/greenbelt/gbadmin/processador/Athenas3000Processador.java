/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Thiago
 */
public class Athenas3000Processador extends Processador{
    
    private static final Logger LOGGER = Logger.getLogger(Athenas3000Processador.class.getName());
    
    private static final Pattern PATTERN_CODIGO = Pattern.compile("^([0-9]{1,} )");
    private static final Pattern PATTERN_QUANTIDADE = Pattern.compile("( [0-9]{1,} )");
    private static final Pattern PATTERN_VALOR = Pattern.compile("([0-9\\.]{1,},[0-9]{2})$");
    
    @Override
    public Rubrica processarLinha(String linha) {
        
        LOGGER.log(Level.INFO, linha);
        
        Matcher matcherCodigo = PATTERN_CODIGO.matcher(linha);
        
        if(!matcherCodigo.find()){
            return null;
        }
        
        String codigo = matcherCodigo.group(1);
        
        linha = StringUtils.substringAfter(linha, codigo);
        
        linha = linha.replace(" %", "%");
        
        Matcher matcherQuantidade = PATTERN_QUANTIDADE.matcher(linha);
        
        matcherQuantidade.find();
        
        String quantidade = matcherQuantidade.group(1);
        
        String descricao = StringUtils.substringBefore(linha, quantidade);
        
        descricao = descricao.replaceFirst("\\*", "");
        
        linha = StringUtils.substringAfter(linha, quantidade);
        
        Matcher matcherValor = PATTERN_VALOR.matcher(linha);
        
        matcherValor.find();
        
        String valor = matcherValor.group();
        
        return new Rubrica(codigo, descricao, valor);
    }
    
    @Override
    public String getInicioBloco() {
        return "Resumo por Verba";
    }

    @Override
    public String getFinalBloco() {
        return "Total do Grupo:";
    }

    @Override
    public int getLinhasAposBloco() {
        return 3;
    }
}