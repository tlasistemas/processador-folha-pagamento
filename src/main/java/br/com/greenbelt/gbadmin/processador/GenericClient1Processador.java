package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class GenericClient1Processador extends Processador {

    private static final Logger LOGGER = Logger.getLogger(GenericClient1Processador.class.getName());
    
    private static final Pattern PATTERN_CODIGO = Pattern.compile("^([0-9]{3}\\s)");
    private static final Pattern PATTERN_VALOR = Pattern.compile("([0-9\\.]{1,},[0-9]{2})");
    
    @Override
    public String getInicioBloco() {
        return ">>>>>> R E S U M O G E R A L >>>>>>";
    }

    @Override
    public String getFinalBloco() {
        return "TOTAL DE FUNCIONÁRIOS ------->";
    }

    @Override
    public int getLinhasAposBloco() {
        return 2;
    }

    @Override
    public Rubrica processarLinha(String linha) {
        LOGGER.info(linha);
        linha = StringUtils.trim(linha);
        
        Matcher matcherCodigo = PATTERN_CODIGO.matcher(linha);
        
        if(!matcherCodigo.find()){
            return null;
        }
        
        String codigo = StringUtils.trim(matcherCodigo.group(1));
        LOGGER.log(Level.INFO, "Código: {0}", codigo);
        linha = linha.substring(codigo.length());
        
        String[] parts = linha.split("\\s{3,}");
        
        String descricao = StringUtils.trim(parts[0]);
        LOGGER.log(Level.INFO, "Descrição: {0}", descricao);
        
        String valor = StringUtils.trim(parts[parts.length - 1]);
        LOGGER.log(Level.INFO, "Valor: {0}", valor);
        
        return new Rubrica(codigo, descricao, valor);
    }
}
