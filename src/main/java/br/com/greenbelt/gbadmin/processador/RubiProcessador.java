/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.text.PDFTextStripperByArea;

/**
 *
 * @author Thiago
 */
public class RubiProcessador extends Processador {

    private static final Logger LOGGER = Logger.getLogger(RubiProcessador.class.getName());

    private static final Pattern PATTERN_CODIGO = Pattern.compile("^([0-9]{1,} )");
    private static final Pattern PATTERN_VALOR = Pattern.compile("([0-9\\.]{1,},[0-9]{2})");
    
    @Override
    protected String getInicioBloco() {
        return "Resumo dos Eventos dos Colaboradores";
    }

    @Override
    protected String getFinalBloco() {
        return "Legenda de incidência dos eventos";
    }

    @Override
    protected int getLinhasAposBloco() {
        return 4;
    }
    
    @Override
    protected String extractPdfContent(PDDocument document) throws IOException {

        StringBuilder pdfContent = new StringBuilder();

        for (PDPage page : document.getPages()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();

            stripper.setSortByPosition(true);

            PDRectangle pdr = page.getCropBox();
            Rectangle rect = new Rectangle();
            rect.setBounds(
                    Math.round(pdr.getLowerLeftX()), 
                    Math.round(pdr.getLowerLeftY()), 
                    Math.round(pdr.getWidth()/2), 
                    Math.round(pdr.getHeight()));

            stripper.addRegion("proventos", rect);

            stripper.extractRegions(page);

            pdfContent.append(stripper.getTextForRegion("proventos"));

            pdfContent.append("\n");
        }

        return pdfContent.toString();
    }

    @Override
    protected Rubrica processarLinha(String linha) {

        LOGGER.log(Level.INFO, linha);
        
        linha = linha.trim();
        
        Matcher matcherCodigo = PATTERN_CODIGO.matcher(linha);
        
        if(!matcherCodigo.find()){
            return null;
        }
        
        String codigo = matcherCodigo.group(1);
        
        linha = StringUtils.substringAfter(linha, codigo);
        
        Matcher matcherValor = PATTERN_VALOR.matcher(linha);
        
        if(matcherValor.find()){
        
            String valor = matcherValor.group(1);

            String descricao = StringUtils.substringBefore(linha, valor);

            if(matcherValor.find()){            
                valor = matcherValor.group(1);        
            }

            return new Rubrica(codigo.trim(), descricao.trim(), valor);
        }
        
        return null;
    }
}
