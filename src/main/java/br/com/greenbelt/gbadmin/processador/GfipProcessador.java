/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Thiago
 */
public class GfipProcessador extends Processador {

    private static final Logger LOGGER = Logger.getLogger(GfipProcessador.class.getName());

    private static final Pattern PATTERN_VALOR = Pattern.compile("([0-9\\.]{1,},[0-9]{2})");
    private static final Pattern PATTERN_EMPTY = Pattern.compile("([\\-]{10,})");
    
    @Override
    protected String getInicioBloco() {
        return "APURAÇÃO DO VALOR A RECOLHER";
    }

    @Override
    protected String getFinalBloco() {
        return "Os valores de retenção";
    }

    @Override
    protected int getLinhasAposBloco() {
        return 2;
    }
    
    @Override
    protected Rubrica processarLinha(String linha) {

        LOGGER.log(Level.INFO, linha);
        
        linha = linha.trim();
        
        String codigo = "";
        String descricao = "";
        String valor = "";
        
        Matcher matcher = PATTERN_VALOR.matcher(linha);
        
        if(matcher.find()){
            
            descricao = StringUtils.substringBefore(linha, matcher.group(1));
            
            matcher.find(5);
            
            valor = matcher.group(1);
            
        } else {
            
            codigo = linha;
            
            if(PATTERN_EMPTY.matcher(codigo).matches()){
                codigo = "";
                descricao = "";
                valor = "";
            }
        }
        
        return new Rubrica(codigo, descricao, valor);
    }
}
