package br.com.greenbelt.gbadmin.processador;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MicrosigaProcessador extends Processador {

    private static final Logger LOGGER = Logger.getLogger(MicrosigaProcessador.class.getName());
    
    private static final Pattern PATTERN_CODIGO = Pattern.compile("^([0-9]\\s[0-9]\\s[0-9]\\s)");
    private static final Pattern PATTERN_VALOR = Pattern.compile("([0-9\\.]{1,},[0-9]{2})");
    
    @Override
    public String getInicioBloco() {
        return "G r u p o d e E m p r e s a s : ";
    }

    @Override
    public String getFinalBloco() {
        return "T O T A I S - >";
    }

    @Override
    public int getLinhasAposBloco() {
        return 4;
    }

    @Override
    public Rubrica processarLinha(String linha) {
        
        Matcher matcherCodigo = PATTERN_CODIGO.matcher(linha);
        
        if(!matcherCodigo.find()){
            return null;
        }
        
        String codigo = matcherCodigo.group(1);
        String descricao = linha.substring(codigo.length(), linha.indexOf("|")-4);
        
        LOGGER.log(Level.INFO, descricao);
        
        String[] strings = descricao.split("\\s{3,}");
        
        descricao = strings[0];
        descricao = descricao.replaceAll("([\\s])([^\\s])", "$2");
        
//        Matcher matcherCodigo = PATTERN_CODIGO.matcher(linha);
//        Matcher matcherValor = PATTERN_VALOR.matcher(linha);
//        
//        if(!matcherCodigo.find()){
//            return null;
//        }
//        
//        String codigo = matcherCodigo.group(1);
//        
//        String descricao = linha.substring(codigo.length());

        codigo = codigo.replaceAll(" ", "");
        String valor = strings[2].replaceAll(" ", "");
        
        return new Rubrica(codigo, descricao, valor);
    }
}
