/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.exporter;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 *
 * @author Thiago
 */
public class CsvExporter {

    private final StringBuilder title = new StringBuilder();
    private final StringBuilder csvContent = new StringBuilder();
    
    public void append(String fileName, List<Rubrica> rubricas){
        
        Double total = 0.0;
        
        StringBuilder stringBuilder = new StringBuilder();
    
        for(Rubrica rubrica : rubricas){
            
            stringBuilder.append(rubrica.getCodigo());
            stringBuilder.append(";");
            
            stringBuilder.append(rubrica.getDescricao());
            stringBuilder.append(";");
            
            stringBuilder.append(rubrica.getValor());
            stringBuilder.append("\n");
            
            total+= rubrica.getValorNumeral();
        }
        
        stringBuilder.append("\n");
        
        this.csvContent.append(fileName);
        this.csvContent.append(";;");
        this.csvContent.append(new DecimalFormat("#.##").format(total));
        this.csvContent.append("\n");
        this.csvContent.append(stringBuilder);
    } 
    
    public String getContent(){
        return csvContent.toString();
    }
}
