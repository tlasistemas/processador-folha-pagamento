/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.exporter;

import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.util.List;

/**
 *
 * @author Thiago
 */
public class ArquivoProcessado {

    private final String referencia;
    
    private final List<Rubrica> rubricas;

    public ArquivoProcessado(String referencia, List<Rubrica> rubricas) {
        this.referencia = referencia;
        this.rubricas = rubricas;
    }

    public List<Rubrica> getRubricas() {
        return rubricas;
    }

    public String getReferencia() {
        return referencia;
    }
}
