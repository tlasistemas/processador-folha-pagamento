/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.controller.param;

import br.com.greenbelt.gbadmin.processador.GfipProcessador;
import br.com.greenbelt.gbadmin.processador.HumanistProcessador;
import br.com.greenbelt.gbadmin.processador.Processador;
import br.com.greenbelt.gbadmin.processador.ProsoftProcessador;
import br.com.greenbelt.gbadmin.processador.RubiProcessador;
import br.com.greenbelt.gbadmin.processador.Athenas3000Processador;
import br.com.greenbelt.gbadmin.processador.GenericClient1Processador;
import br.com.greenbelt.gbadmin.processador.MicrosigaProcessador;

/**
 *
 * @author Thiago
 */
public enum Modelo {
    
    PROSOFT("Prosoft"){

        @Override
        public Processador factory() {
            return new ProsoftProcessador();
        }
        
    }, HUMANIST("Humanist"){

        @Override
        public Processador factory() {
            return new HumanistProcessador();
        }
    }, RUBI("Rubi"){
        
        @Override
        public Processador factory() {
            return new RubiProcessador();
        }
    }, GFIP("Gfip"){
        
        @Override
        public Processador factory() {
            return new GfipProcessador();
        }
    }, ATHENAS_3000("Athenas 3000"){
        
        @Override
        public Processador factory() {
            return new Athenas3000Processador();
        }
    }, MICROSIGA("Microsiga"){
        
        @Override
        public Processador factory() {
            return new MicrosigaProcessador();
        }
    }, GENERICO_CLIENT_1("Cliente MS Consultória 1"){
        
        @Override
        public Processador factory() {
            return new GenericClient1Processador();
        }
    };
    
    private final String descricao;

    private Modelo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public abstract Processador factory();
}
