/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.greenbelt.gbadmin.controller.param;

/**
 *
 * @author Thiago
 */
public class Processamento {

    private Modelo modelo;
    
    private Boolean gerarCsv;

    public void setGerarCsv(Boolean gerarCsv) {
        this.gerarCsv = gerarCsv;
    }

    public Boolean getGerarCsv() {
        return gerarCsv;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }
}
