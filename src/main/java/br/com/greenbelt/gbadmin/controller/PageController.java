package br.com.greenbelt.gbadmin.controller;


import br.com.greenbelt.gbadmin.controller.param.Processamento;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.observer.download.Download;
import br.com.caelum.vraptor.observer.download.DownloadBuilder;
import br.com.caelum.vraptor.observer.upload.UploadSizeLimit;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.greenbelt.gbadmin.exporter.CsvExporter;
import br.com.greenbelt.gbadmin.processador.Processador;
import br.com.greenbelt.gbadmin.processador.modelo.Rubrica;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

@Controller
public class PageController {
    
    private static final Logger LOGGER = Logger.getLogger(PageController.class.getName());
    
    private Result result;
    
    public PageController() {
    }

    @Inject
    public PageController(Result result) {
        this.result = result;
    }
    
    @Get({"/home", "/index", "/"})
    public void home() {
    }
    
    @Post("/upload-folha-pagamento")
    @UploadSizeLimit(sizeLimit = 40 * 1024 * 1024, fileSizeLimit = 10 * 1024 * 1024)
    public Download upload(Processamento processamento, List<UploadedFile> folhasPagamento) {
        
        try {
            
            Processador processador = processamento.getModelo().factory();
    
            if(processamento.getGerarCsv()){
            
                CsvExporter csvExporter = new CsvExporter();
                
                for(UploadedFile uploadedFile : folhasPagamento){
                    
                    List<Rubrica> rubricas = processador.processar(uploadedFile);
                    
                    csvExporter.append(uploadedFile.getFileName(), rubricas);
                }
            
                return criarDownload(csvExporter.getContent(), processamento);
                
            } else {
                
                List<Rubrica> rubricas = new ArrayList<>();
                
                for(UploadedFile uploadedFile : folhasPagamento){
                    
                    rubricas.add(new Rubrica(uploadedFile.getFileName(), "", ""));
                    rubricas.addAll(processador.processar(uploadedFile));
                    
                }
                
                result.include("rubricas", rubricas);
            }
            
        } catch (IOException ex) {
            
            LOGGER.log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    private Download criarDownload(String conteudo, Processamento processamento) {
        
        DownloadBuilder.ByteArrayDownloadBuilder downloadBuilder = DownloadBuilder.of(conteudo.getBytes());
        
        downloadBuilder.withFileName("processador_" + processamento.getModelo() + ".csv");
        downloadBuilder.withContentType("text/plain");
        downloadBuilder.downloadable();
        
        return downloadBuilder.build();
    }
}