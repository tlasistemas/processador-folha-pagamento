<%@page import="br.com.greenbelt.gbadmin.controller.param.Modelo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<form role="form" method="post" action="${pageContext.request.contextPath}/upload-folha-pagamento" enctype="multipart/form-data">

    <div class="form-group">
        <label class="control-label">Modelo</label>
        <div>
            <select class="form-control" name="processamento.modelo" required>
                <option value=""></option>
                <c:forEach items="<%=Modelo.values()%>" var="item">
                    <option value="${item}">${item.descricao}</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label">Gerar CSV</label>
        <div>
            <div class="radio-inline">
                <label><input type="radio" name="processamento.gerarCsv" value="0" /> N�o</label>
            </div>
            <div class="radio-inline">
                <label><input type="radio" name="processamento.gerarCsv" value="1" checked/> Sim</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label">Arquivos</label>
        <div>
            <input name="folhasPagamento[]" type="file" multiple required/>
        </div>        
    </div>   

    <div class="form-group">       
        <div>
            <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-refresh"></i> PROCESSAR</button>                
        </div>
    </div>
</form>