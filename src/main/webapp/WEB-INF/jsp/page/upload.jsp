<%-- 
    Document   : uplaod
    Created on : 28/05/2016, 16:44:32
    Author     : Thiago
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Arquivo Processado</title>
    </head>
    <body>
        <h1>Resumo Geral da Empresa</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th class="text-right">Valor</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${rubricas}" var="item">
                    <tr>
                        <td>${item.codigo}</td>
                        <td>${item.descricao}</td>
                        <td class="text-right">${item.valor}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
