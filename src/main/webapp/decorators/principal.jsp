<%@page contentType="text/html; charset=UTF-8" %> 
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><decorator:title default="Green Belt - Processador de Folha de Pagamento" /></title>
        <link href="${pageContext.request.contextPath}/assets/normalize/normalize.css" rel="stylesheet" type="text/css">
        <link href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="${pageContext.request.contextPath}/assets/bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/app/card.css" rel="stylesheet">   
        <link href="${pageContext.request.contextPath}/assets/bootstrap/css/sb-admin-2.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/assets/bootstrap/css/timeline.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">        
        <!--[if lt IE 9]>
                <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--        <link href="/gbadmin/css/styles.css" rel="stylesheet">-->                        
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/jquery/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/moment/moment.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/bootstrap/js/jasny-bootstrap.min.js"></script>                
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap-datetimepicker.js"></script>        
        <decorator:head />
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="${pageContext.request.contextPath}/home"><i class="glyphicon glyphicon-home"></i> Green Belt Tecnologia</a>
                    </div>
                </nav>
            </div>                        

            <div class="row">
                <div id="mainContainer" class="col-lg-12">
                    <decorator:body />
                </div>
            </div>
        </div>    
    </body>
</html>